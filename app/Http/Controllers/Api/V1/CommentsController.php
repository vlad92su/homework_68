<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\CommentStoreRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class CommentsController extends ApiV1Controller
{

    /**
     * ArticlesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index']);
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $comments = Comment::paginate(8);
        return CommentResource::collection($comments);
    }



    /**
     * Store a newly created resource in storage.
     */
    public function store(CommentStoreRequest $request)
    {
        $comment = Comment::create($request->validated());
        return new CommentResource($comment);
    }


    /**
     * @param string $id
     * @return CommentResource|JsonResponse
     */
    public function show(string $id)
    {
        $comment = Comment::find($id);
        if ($comment) {
            $comment->with('user');
            $comment->makeVisible(['created_at', 'updated_at']);
            return new CommentResource($comment);
        }
        return response()->json([
            'data' => null,
            'message' => 'not found'
        ], 404);
    }


    /**
     * @param Request $request
     * @param string $id
     * @return CommentResource
     */
    public function update(Request $request, string $id)
    {
        $comment = Comment::find($id);
        $comment->update($request->all());
        return new CommentResource($comment);
    }


    /**
     * @param string $id
     * @return Application|ResponseFactory|\Illuminate\Foundation\Application|Response
     */
    public function destroy(string $id)
    {
        $comment = Comment::find($id);
        $comment->delete();
        return response('', 204);
    }
}
