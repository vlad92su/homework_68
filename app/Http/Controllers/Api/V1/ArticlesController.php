<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Articles\ArticleStoreRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class ArticlesController extends ApiV1Controller
{

    /**
     * ArticlesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index']);
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $articles = Article::paginate(8);
        return ArticleResource::collection($articles);
    }

    /**
     * @param ArticleStoreRequest $request
     * @return ArticleResource
     */
    public function store(ArticleStoreRequest $request)
    {
        $article = Article::create($request->validated());
        return new ArticleResource($article);
    }


    /**
     * @param string $id
     * @return ArticleResource|JsonResponse
     */
    public function show(string $id)
    {
        $article = Article::find($id);
        if ($article) {
            $article->with('user');
            $article->makeVisible(['created_at', 'updated_at']);
            return new ArticleResource($article);
        }
        return response()->json([
            'data' => null,
            'message' => 'not found'
        ], 404);
    }


    /**
     * @param Request $request
     * @param string $id
     * @return ArticleResource
     */
    public function update(Request $request, string $id)
    {
        $article = Article::find($id);
        $article->update($request->all());
        return new ArticleResource($article);
    }


    /**
     * @param string $id
     * @return Application|ResponseFactory|\Illuminate\Foundation\Application|Response
     */
    public function destroy(string $id)
    {
        $article = Article::find($id);
        $article->delete();
        return response('', 204);
    }
}
