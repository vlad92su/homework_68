<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Passport\Passport;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class ArticlesTest extends TestCase
{
    use DatabaseMigrations;

    private $user;
    private $articles;
    private string $url;

    public const DATA = [
        'title' => 'test',
        'body' => 'body test',
        'user_id' => null
    ];

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->url = route('articles.store');
        $this->articles = \App\Models\Article::factory()->count(8)->for($this->user)->create();
    }

    /**
     * @group articles
     */
    public function test_can_get_all_articles()
    {
        $request =$this->getJson(route('articles.index'));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertCount($this->articles->count(), $payload->data);
    }


    /**
     * @group articles
     */
    public function test_success_create_article(): void  //успешное созданте статьи
    {
        $data = self::DATA;
        Passport::actingAs(
            $this->user,
            [route('articles.store')]
        );
        $data['user_id'] = $this->user->id;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(201);
        $response->assertJsonStructure(
            [
                'data' => [
                    'title', 'body', 'id', 'user'
                ]
            ]
        );

        $this->assertDatabaseHas('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_title(): void  //протестируйте попытку создать статью без названия
    {
        $data = self::DATA;
        Passport::actingAs(
            $this->user,
            [route('articles.store')]
        );
        $data['user_id'] = $this->user->id;
        $data['title'] = null;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'title'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_body(): void  //протестируйте попытку создать статью без тела
    {
        $data = self::DATA;
        Passport::actingAs(
            $this->user,
            [route('articles.store')]
        );
        $data['user_id'] = $this->user->id;
        $data['body'] = null;
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'body'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_user_id(): void  //протестируйте попытку создать статью без идентификатора пользователя
    {
        $data = self::DATA;
        Passport::actingAs(
            $this->user,
            [route('articles.store')]
        );
        $this->assertDatabaseMissing('articles', $data);
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'user_id'
                ]
            ]
        );

        $this->assertDatabaseMissing('articles', $data);
    }

    /**
     * @group articles
     */
    public function test_trying_create_article_without_all_data(): void   //протестируйте попытку создать статью без всех данных
    {
        $data = [];
        Passport::actingAs(
            $this->user,
            [route('articles.store')]
        );
        $response = $this->postJson($this->url, $data);
        $response->assertStatus(422);
        $response->assertJsonStructure(
            [
                'message',
                'errors' => [
                    'title',
                    'body',
                    'user_id'
                ]
            ]
        );
    }

    /**
     * Failed get article.
     * @group articles
     * @return void
     */
    public function test_get_article()
    {
        $article = $this->articles->random();
        $request =$this->getJson(route('articles.show', ['article' => $article]));
        $request->assertUnauthorized();
    }

    /**
     * Success get article.
     * @group articles
     * @return void
     */
    public function test_success_get_article()
    {
        $article = $this->articles->first();
        Passport::actingAs(
            $this->user,
            [route('articles.show', ['article' => $article])]
        );
        $request =$this->getJson(route('articles.show', ['article' => $article]));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertArrayHasKey('id', (array)$payload->data);
        $this->assertEquals($article->id, $payload->data->id);
        $this->assertArrayHasKey('title', (array)$payload->data);
        $this->assertEquals($article->title, $payload->data->title);    }

    /**
     * Success update article.
     * @group articles
     * @return void
     */
    public function test_success_update_article()
    {

        $data = self::DATA;
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.update',['article' => $article])]
        );
        $data['user_id'] = $this->user->id;
        $response = $this->putJson(route('articles.update', ['article' => $article->id]));

        $response->assertStatus(200);
    }
    /**
     * Failed update article.
     * @group articles
     * @return void
     */
    public function test_content_validation_error()
    {
        $article = $this->articles->random();
        $data = self::DATA;
        Passport::actingAs(
            $this->user,
            [route('articles.update', ['article' => $article])]
        );
        $data['user_id'] = $this->user->id;
        $data['title'] = '';
        $request = $this->postJson(route('articles.store', ['article' => $article]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('title', (array)$payload->errors);
    }

    /**
     * Success destroy article.
     * @group articles
     * @return void
     */
    public function test_success_destroy_article()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('articles.destroy',['article' => $article])]
        );
        $response = $this->json('delete', route('articles.destroy', $article));
        $response->assertNoContent();
    }

    /**
     * Failed destroy article.
     * @group articles
     * @return void
     */
    public function test_failed_destroy_article()
    {
        $article = $this->articles->random();
        $response = $this->json('delete', route('articles.destroy', $article));
        $response->assertUnauthorized();;
    }



}
