<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CommentTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var Collection|User
     */
    private $user;

    /**
     * @var Collection|Article
     */
    private $article;

    /**
     * @var Collection|Article[]
     */
    private $articles;

    /**
     * @var Collection|Comment
     */
    private $comment;

    /**
     * @var Collection|Comment[]
     */
    private $comments;

    public const DATA = [
        'body' => 'body test',
        'article_id' => null,
        'user_id' => null
    ];

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->articles = Article::factory()->count(8)->for($this->user)->create();
        $this->article = $this->articles->first();
        $this->comments = Comment::factory()->count(100)->for($this->user)->for($this->article)->create();
    }

    /**
     * A basic feature test example.
     */
    public function test_example(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }


    /**
     * Success get all comment.
     * @group comments
     * @return void
     */
    public function test_can_get_all_comments()
    {
        $request =$this->getJson(route('comments.index'));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertCount($this->articles->count(), $payload->data);
    }


    /**
     * Success create comment.
     * @group comments
     * @return void
     */
    public function test_can_create_comment()
    {
        $article = $this->articles->random();
        Passport::actingAs(
            $this->user,
            [route('comments.store',  ['article' => $article])]
        );
        $user = \App\Models\User::factory()->create();
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['article_id'] = $this->article->id;
        $request = $this->postJson(route('comments.store', ['article' => $article]), $data);
        $payload = json_decode($request->getContent());
        $request->assertCreated();
        $this->assertDatabaseHas('comments', $data);
        $this->assertDatabaseHas('comments', ['id' => $payload->data->id]);
    }

    /**
     * Failed create article.
     * @group comments
     * @return void
     */
    public function test_title_validation_error()
    {
        $article = $this->articles->random();
        $data = [
            'user_id' => $this->user->id,
            'article_id' => $this->user->id,
        ];
        Passport::actingAs(
            $this->user,
            [route('comments.store',['article' => $article])]
        );
        $request = $this->postJson(route('comments.store', ['article' => $article]), $data);
        $payload = json_decode($request->getContent());
        $request->assertStatus(422);
        $this->assertArrayHasKey('message', (array)$payload);
        $this->assertArrayHasKey('errors', (array)$payload);
        $this->assertArrayHasKey('body', (array)$payload->errors);
    }

    /**
     * Failed get comment.
     * @group comments
     * @return void
     */
    public function test_get_comment()
    {
        $comment = $this->comments->random();
        $request =$this->getJson(route('comments.show', ['comment' => $comment]));
        $request->assertUnauthorized();
    }

    /**
     * Success get comment.
     * @group comments
     * @return void
     */
    public function test_success_get_comment()
    {
        $comment = $this->comments->random();
        Passport::actingAs(
            $this->user,
            [route('comments.show', ['comment' => $comment])]
        );
        $request =$this->getJson(route('comments.show', ['comment' => $comment]));
        $request->assertSuccessful();
        $payload = json_decode($request->getContent());
        $this->assertArrayHasKey('data', (array)$payload);
        $this->assertArrayHasKey('id', (array)$payload->data);
        $this->assertEquals($comment->id, $payload->data->id);
        $this->assertArrayHasKey('body', (array)$payload->data);
        $this->assertEquals($comment->body, $payload->data->body);
    }

    /**
     * Success update comment.
     * @group comments
     * @return void
     */
    public function test_comment_update()
    {
        $comment = $this->comments->random();
        $article = $this->articles->random();
        $data = self::DATA;
        $data['user_id'] = $this->user->id;
        $data['article_id'] = $this->article->id;
        Passport::actingAs(
            $this->user,
            [route('comments.update', ['comment' => $comment, 'article' =>$article])]
        );
        $request = $this->postJson(route('comments.store', ['comment' => $comment, 'article' =>$article]), $data);
        $request->assertStatus(201);
    }

    /**
     * Failed update comment.
     * @group comments
     * @return void
     */
    public function test_failed_update_comment()
    {
        $comment = $this->comments->random();
        $response = $this->json('put',route('comments.update', ['comment' => $comment]));
        $response->assertUnauthorized();
    }

    /**
     * Success destroy comment.
     * @group comments
     * @return void
     */
    public function test_success_destroy_comment()
    {
        $comment = $this->comments->random();
        Passport::actingAs(
            $this->user,
            [route('comments.destroy',['comment' => $comment])]
        );
        $response = $this->json('delete', route('comments.destroy', $comment));
        $response->assertNoContent();
    }

    /**
     * Failed destroy comment.
     * @group comments
     * @return void
     */
    public function test_failed_destroy_comment()
    {
        $comment = $this->comments->random();
        $response = $this->json('delete', route('comments.destroy', $comment));
        $response->assertUnauthorized();
    }


}
