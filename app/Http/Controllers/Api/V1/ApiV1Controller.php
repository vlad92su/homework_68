<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class ApiV1Controller extends Controller
{
    protected function success(array|Collection|Model $data = [])
    {
        return response()->json([
            'data' => $data,
            'message' => 'success'
        ]);
    }
}
